import React, { useState } from "react"
import CanvasJSReact from "../assets/canvasjs.react"
// import queryString from "query-string"
// import { Location } from "@reach/router"
import { Slider, makeStyles, Typography } from "@material-ui/core"

const useStyles = makeStyles({
  root: {
    width: 600,
  },
})

console.log("invalidating the cache")
var cache = {}

function stringRep(num, bitlength) {
  return num.toString(2).padStart(bitlength, "0")
}

function numTests(num, bitlength) {
  let str = stringRep(num, bitlength)
  if (cache[str]) {
    return cache[str]
  }
  if (bitlength === 1) return 1
  else if (num === 0) return 1
  const lower = Math.ceil(bitlength / 2)
  const upper = bitlength - lower
  const lowerNum = num & (Math.pow(2, lower) - 1)
  const upperNum = num >> lower
  //   console.log(
  //     num.toString(2),
  //     "(",
  //     bitlength,
  //     ")",
  //     "->",
  //     upperNum.toString(2),
  //     "(",
  //     upper,
  //     ")",
  //     ",",
  //     lowerNum.toString(2),
  //     "(",
  //     lower,
  //     ")"
  //  )
  const answer = 1 + numTests(upperNum, upper) + numTests(lowerNum, lower)
  cache[str] = answer
  return answer
}

function calculateProbability(num, length, prob) {
  let result = 1
  for (let i = 0; i < length; ++i) {
    if (num & Math.pow(2, i)) result = result * prob
    else result = result * (1 - prob)
  }
  return result
}

function calculateFinalNumTests(bulk, prob) {
  let finalNumTest = 0
  for (let i = 0; i < Math.pow(2, bulk); ++i) {
    const numT = numTests(i, bulk)
    const iProb = calculateProbability(i, bulk, prob)
    // console.log(i, numT, iProb)
    finalNumTest += iProb * numT
  }
  return finalNumTest
}

function Index() {
  const classes = useStyles()

  const [infectedPercent, setInfectedPercent] = useState(2)

  //   const num = numTests(1978, 20)
  //   console.log("total number of tests", num)
  let dataPoints = []
  const prob = infectedPercent / 100
  const maxBulk = 16
  for (let bulk = 1; bulk <= maxBulk; bulk++) {
    let finalNumTest = calculateFinalNumTests(bulk, prob)
    dataPoints = [...dataPoints, { x: bulk, y: finalNumTest / bulk }]
  }
  console.log(dataPoints)

  const CanvasJS = CanvasJSReact.CanvasJS
  const CanvasJSChart = CanvasJSReact.CanvasJSChart

  const options = {
    animationEnabled: true,
    exportEnabled: true,
    theme: "light2", // "light1", "dark1", "dark2"
    title: {
      text: "Number of tests per person as test bulk size varies",
    },
    axisY: {
      title: "Expected number of tests per person",
      includeZero: false,
      suffix: "",
    },
    axisX: {
      title: "Bulk size",
      prefix: "",
      interval: 1,
    },
    data: [
      {
        type: "line",
        toolTipContent: "Bulk test {x}: expected tests {y}",
        dataPoints: dataPoints,
      },
    ],
  }

  function valuetext(value) {
    return `${value}`
  }

  const onSliderChange = (event, value) => {
    console.log(value)
    setInfectedPercent(value)
    cache = {}
    console.log({ cache })
  }

  return (
    <div>
      <div className={classes.root}>
        <Typography id="discrete-slider-small-steps" gutterBottom>
          Estimate of fraction of infected population
        </Typography>
        <Slider
          value={infectedPercent}
          getAriaValueText={valuetext}
          aria-labelledby="discrete-slider-small-steps"
          step={1}
          marks
          min={1}
          max={40}
          valueLabelDisplay="auto"
          onChange={onSliderChange}
        />
      </div>
      {/* <Location>
        {({ location, navigate }) => {
          console.log(location)
          return <div>Hello World!</div>
        }}
      </Location> */}
      <CanvasJSChart
        options={options}
        /* onRef = {ref => this.chart = ref} */
      />
    </div>
  )
}

export default Index
